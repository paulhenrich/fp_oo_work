;; Anything you type in here will be executed 
;; immediately with the results shown on the 
;; right.

(def second (fn [x] (first (rest x))))
(second [1 2 3 4 5])

(defn third [x] (first (rest (rest x))))

(third [1 2 3 4])

(defn my-apply [function sequence] (eval (cons function sequence)))


(* 5 5)
(defn square [a] (* a a))

(defn add-squares [& nums]
  (apply + (map square nums)))

(add-squares 1 2 3)

(defn bizarro-factorial [x]
  (apply * (range 1 (+ x 1))))
(bizarro-factorial 5)




(take 5 '(1 2 3 4 5 6))

(defn offset-take [offset n seq]
  (take n (drop offset seq)))
(offset-take 2 5 [1 2 3 4 5 6 7 ])

;; Grr, lighttable crash
;; additional built-ins practice
(defn distinct-sum [& nums]
  (apply + (distinct nums)))
(defn distinct-concat [& lists]
  (distinct (apply concat lists)))
(defn repeat-by-size [list]
  (repeat (count list) list))
(defn square-dots [x]
  (repeat (square x) '.))
(defn interleave-doubles [list]
  (interleave list (map (fn [x] (* 2 x)) list)))
(defn hash-doubles [list]
  (apply hash-map (interleave-doubles list)))

(hash-map 1 2 3 4)

(distinct-sum 1 2 2 2 2 5)
(distinct-concat [1 2 3] [1 2 3 4 5])
(repeat-by-size [1 2 3 4])
(square-dots 2)
(interleave-doubles [2  10])
(hash-doubles [1 2 3 4])



;; ch 1 built-ins continued

(drop 4 [1 2 3 4 5 6])
(drop-last 12 [1 2 3 4 5 6 7 7 6 6 6 6 6 6 66])
(defn pop-ends [list]
    (drop-last 1 (drop 1 list)))

(pop-ends [10 1 10 20 30 40 50])





(partition 5 [1 2 3 4 5 6])

(defn multiply [list]
  (eval (flatten (concat '(*) list))))

(multiply [1 2 3 4 ])
(defn multiply-partition [n list]
  (map multiply (partition n list)))

(multiply-partition 2 [1 2 3 4])

;; ex 6
(defn prefix-of? [candidate list]
  (=  candidate
     (take (count candidate) list) candidate))

(prefix-of? '(1 2 3) [1 2 3 4])
(prefix-of? [1 2 3 3] [1 2 3 4 5])

;; trying out some recursion for ex 7
;; hmmm
(defn tails
  ([list list-accum]
    (if (empty? list)
      list-accum
      (recur (drop 1 list) (cons list-accum (drop 1 list))))))
;; almost
(tails [1 2 3 4] [])

;; using map hint:
(defn tails2 [list]
  (map drop
       (range 0 (inc (count list)))
       (repeat (inc (count list)) list)))

(tails2 '(1 2 3 4 5))


(def puzzle (fn [argu] (list argu)))

(puzzle '(1 2 3))













