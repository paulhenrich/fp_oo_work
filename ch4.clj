(defn make [klass & args]
  (apply klass args))

(defn send-to [this method & arguments]
  (apply
   (method (:__methods__ this))
   this arguments))
;; Don't litter

(defn Point [x y]
  {:x x
   :y y
   
   ;; meta
   :__class_symbol__ 'Point
   :__methods__ {
                 :class :__class_symbol__
                 
                 :x (fn [this] (:x this))
                 :y (fn [this] (:y this))
                 :shift (fn [this xinc yinc]
                          (make Point (+ (send-to this :x) xinc) (+ (send-to this :y) yinc)))

                 }})


(make Point 1 2)


(def point (make Point 1 2))



(send-to point :shift 500 250)

