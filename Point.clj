(defn make [klass & args]
  (apply klass args))

;; Point

(defn Point [x y]
  {:x x
   :y y
   :__class_symbol__ 'Point})

(def class-of :__class_symbol__)
(def x :x)
(def y :y)

(defn shift [this xinc yinc]
  (Point (+ (x this) xinc)
         (+ (y this) yinc)))

(defn add [this point]
  (shift this (x point) (y point)))


(make Point 1 2)
  
;; Triangle

(defn Triangle [a b c]
  {:a a
   :b b
   :c c
   :__class_symbol__ 'Triangle})

(defn triangle-points [this]
  (select-keys this [:a :b :c]))

(make Triangle
      (Point 0 0)
      (Point 0 5) 
      (Point 10 0))
(make Triangle
      (make Point 1 2)
      (make Point -1 -1)
      (make Point -1 2))

;; example triangles from add-and-make
(def right-triangle (Triangle (Point 0 0)
                              (Point 0 1)
                              (Point 1 0)))

(def equal-right-triangle (Triangle (Point 0 0)
                                    (Point 0 1)
                                    (Point 1 0)))

(def different-triangle (Triangle (Point 0 0)
                                  (Point 0 10)
                                  (Point 10 0)))

(def invalid-triangle (Triangle (Point 0 10)
                                  (Point 0 10)
                                  (Point 10 0)))

(defn equal-triangles? [& triangles]
  (apply = triangles))

(equal-triangles? right-triangle equal-right-triangle)
(equal-triangles? right-triangle different-triangle)
(equal-triangles? right-triangle equal-right-triangle different-triangle)

(defn valid-triangle? [& points]
  (= (count
      (distinct points)
      ) 3))

;;; works but I'd like it to take a triangle
(valid-triangle? (:a right-triangle) (:b right-triangle) (:c right-triangle))

;;; like this, without apply
(apply valid-triangle? (triangle-points right-triangle))















