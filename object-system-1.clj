;; maps
{:a 1, :b 2}
{:a 1 :b 2}

(hash-map :a 1 :b 2)

(def my-map {:a 1, :b 2})

(get my-map :a)
(get my-map :foo)
(:a my-map)
(:foo my-map)

(defn do-callable-to-map [callable]
  (callable {:a 1 :b 2}))

(do-callable-to-map :a)
(do-callable-to-map :b)
(do-callable-to-map :c)
(do-callable-to-map count)
(do-callable-to-map (fn [x] (repeat 2 x)))

(assoc my-map :c 3)
(assoc my-map :foo :bar)
(assoc my-map :a 1) ;; overrides
;; maps have no order? ^^

(merge my-map {:foo :bar})
(merge my-map {:a 2}) ;; overrides

(dissoc my-map :a)
(dissoc my-map :a :b)
(dissoc my-map :a :b :c)

;; an Object!
(def Point
  (fn [x y]
  {:x x, :y y}))

(:x (Point 1 2))

;; a getter
(defn x [this]
  (:x this))

(x (Point 2 4))

;; shorter getters
(def x :x)
(def y :y)
(y (Point 5 2))
(x (Point 5 2))


;; redefine Point to store its class

(defn Point [x y]
  {:x x
   :y y
   :__class_symbol__ 'Point})

(def class-of :__class_symbol__)
(class-of (Point 1 2))

(defn shift [this xinc yinc]
  (Point (+ (x this) xinc)
         (+ (y this) yinc)))

(defn add [this point]
  (Point (+ (x this) (x point))
         (+ (y this) (y point))))

;; some points
(def origin (Point 0 0))
(def point2 (Point 5 7))
(def point3 (Point 2 -1))

(add point2 point3)

;; a better add
(defn add [this point]
  (shift this (x point) (y point)))


(add point2 point3)
(add origin point3)
(shift (Point 1 2) 100 20)




















